
from collections import OrderedDict


types_of_input_mesostructural_data = (
    "planes",
    "axes",
    "planes & axes",
    "fault planes with slickenline trend, plunge and movement sense",
    "fault planes with rake"
)

types_of_azimuth_for_input_plane = [
    "dip direction",
    "strike rhr"
]

types_of_dip_for_input_plane = [
    "dip angle"
]

types_of_axis_rake_for_input_plane = [
    "rake - Aki & Richards, 1980"
]

types_of_azimuth_for_input_axis = [
    "trend"
]

types_of_dip_for_input_axis = [
    "plunge"
]

types_of_movement_sense_input = [
    "mov. sense - N or R"
]

CHOOSE_TEXT = "choose"
UNDEFINED_TEXT = "---"

line_styles = [
    "solid",
    "dashed",
    "dashdot",
    "dotted"
]

marker_styles = OrderedDict(
    [
        ("circle", "o"),
        ("square", "s"),
        ("diamond", "D"),
        ("triangle", "^")
    ]
)

graphic_file_formats = [
    "pdf",
    "png",
    "svg",
    "tif"
]

dpi_resolutions = [200, 400, 600, 800, 1000, 1200]
