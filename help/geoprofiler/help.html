<html>

<head>
<title>GeoProfiler</title>
<style>


h5 { font-size: 95%; color: #333; }
img { max-width: 95%; }

</style>

</head>

<body>

<h1>GeoProfiler vers. 0.1 help</h1>

<div style="font-size: 90%; font-style: italic;">
	Plugin creators: M. Alberti and M. Zanieri.
	<br />The original concept is by M. Zanieri, while the implementation is by M. Alberti.
</div>

<br /><br />

<div>
	The purpose of the experimental <b>geoProfiler</b> module is to help in the creation of geological profiles.<br />Georeferenced data describing topography,
	geological outcrops and attitudes can be used as source data.
	<br /><br />
	This module is a successor to <b>qProf</b>, an independent QGIS plugin, whose code has now been largely
	rewritten and incorporated as a new module into <b>qgSurf</b>.
	<br />The original <b>qProf</b> plugin will continue to exist but only bug-fixes will be applied to it and no new feature will be added.
	<br /><br /><b>geoProfiler</b> is however not at parity with <b>qProf</b> functions, lacking a few features while on the other hand
	has more advanced features for the creation of parallel profiles.
	<br /><br />The most notable limitations of the current <b>geoProfiler</b> version with
	respect to <b>qProf</b> are: a) the inability to handle datasets with different CRS, so that all input data
	must share the same, non-geographic CRS; b) no processing for GPX input data is available; c) no slope calculation is performed.
	<br /><br />Two major improvements are that geological profiles are no longer restricted to straight lines (i.e. just a segment) and parallel profiles are automatically
	created by a single source profile trace.


</div>

<div style="text-align:center;margin: 20px;font-size: 90%;font-style:italic;">
	<img src="images/f01.png" style="align:middle; max_width: 80%"/>
	<br /><br />Fig. 1. The module interface, with the various commands expanded.
</div>


<h2>1. General workflow</h2>

	<div>
		The most important aspect to consider is that in this initial, experimental release <u>all used data must be in
		the same planar CRS</u> (e.g., use  data projected in the same UTM or Lambert projection, not with lat-long coordinates).
		The module in fact do not reproject data between different CRS.
		<br />Considering now the processing workflow, you may consider it to be from top-to-bottom as depicted in the module UI (fig. 1).
		<br /><br />
		As a first step you have to define a profile trace source, by reading it from a loaded line layer or directly drawing it in the map canvas.
		<br />Afterwards you define the input DEM that will be used for the topography representation.
		<br /><br />
		These two steps already allow to plot a profile, that would be a single topographic profile with default parameters (fig. 2).
	</div>

	<div style="text-align:center;margin: 20px;font-size: 90%;font-style:italic;">
		<img src="images/f02.png" style="align:middle; max_width: 80%"/>
		<br /><br />Fig. 2. Example of topographic profile with standard parameters. Data: Aster, Southern Italy (Calabria-
		Basilicata boundary, Thyrrenian coast to Ionian coast.
	</div>
	<br /><br />
	<div>
		<br />Otherwise, you may define the number of parallel profiles to create and their spacing, add geological data and specify
		the graphical parameters to use for the plot. Finally you can export the profiles as point or line shapefiles.
		<br />Geological data can be projected on the profile (e.g., points or geological attitudes) or can intersect the profile (e.g., faults, geological outcrops).
		Geological data are stored in point, line or polygon layers.
		><br />
		In details, these processings can be:
		<ul>
			<li>the projection of points on the profile section;</li>
			<li>the projection of geological attitudes;</li>
			<li>the intersection of geological lines (e.g., faults) on the profile section;</li>
			<li>the intersection of geological polygonal elements (e.g., outcrops) on the profile section.</li>
		</ul>

		The last step consists in exporting created datasets as graphics or GIS data, for further elaborations via graphic or geological software.
	</div>

<h2>2. Initial dataset definitions</h2>

	<h3>2.1 Definition of profile trace</h3>
	<div>
		The first step in the profile creation is the definition of the trace to use. It has to be a single trace, that can be straight or not.
		<br />It can be loaded from a line layer, using "<i>load trace from line layer</i>" under "<i>Profile trace</i>" or can be digitized in the canvas
		with "<i>digitize trace</i>" under "<i>Profile trace/digitize trace in canvas</i>" (fig. 3).
	</div>
	<div>
		A profile line may be digitised directy in the map canvas by using the "<i>Digitize line</i>" button. After activating the button, you add a point with a left click, and terminate the line with a right click.
		It is possible to delete the digitized line by cliking the "<i>clear trace</i>" button. With the "<i>save trace</i>" button it is possible to save the line as a shapefile and add it to the project.
		<br />Note that if you digitize the profile in the canvas, the project CRS must be the same as all the other used data (e..g, the DEM, the
		geological attitudes, and so on).
	</div>

	<div style="text-align:center;margin: 20px;font-size: 90%;font-style:italic;">
		<img src="images/f03.png" style="align:middle; max_width: 80%"/>
		<br /><br />Fig. 3. Definition of the input profile trace.
	</div>
	<br /><br />

	<h3>2.2 Definition of elevation source</h3>

	<div>
	By clicking on "<i>Elevation source</i>" the user can choose which one of the loaded DEMs to use (fig. 4).
	</div>
	<br />
	<div style="text-align:center;margin: 20px;font-size: 90%;font-style:italic;">
		<img src="images/f04.png" style="align:middle; max_width: 80%"/>
		<br /><br />Fig. 4. Choice of source DEMs.
	</div>

	<h3>2.3 Profile parameters</h3>

	<div>
	Having defined both the source profile trace and the DEM, it is possible to define the number of parallel profiles and
		their lateral spacing. By default the number is 1, so if you do not define this parameters the plot will
		consist of just one profile.
		The profile parameters UI is available from double-clicking the "<i>Profiles generation</i>" item (fig.5).
		The parallel profiles will be at the left and right of the base profile, so that the total profile number
		is always an odd number.
	</div>
	<br />
	<div style="text-align:center;margin: 20px;font-size: 90%;font-style:italic;">
		<img src="images/f05.png" style="align:middle; max_width: 80%"/>
		<br /><br />Fig. 5. Definition of the profile parameters. In this example 5 parallel profiles will be created with a spacing of 200 units (depending on the CRS, meters, feet or others).
	</div>

<h2>3. Profile plot</h2>

	<div>

		The profile plot is created by double-clicking the "<i>Plot profiles</i>" item (fig. 3).
		<br />Just the source profile and the DEM definitions are required between plotting the profiles.
		<br />Default plot and graphical parameters will be used unless you define them using the graphical parameters definition ("<i>define graphical parameters</i>" in fig. 1).
		<br />When you modify the graphical parameters or you add geological information by using the items under "<i>Geological data</i>" (fig. 1), the plot will show the updated
		settings and data when the "<i>Plot profiles</i>" item is double-clicked again .
	</div>


<h2>4. Geological data</h2>

	There are various kinds of geological data that can be plotted into profiles.
	Based on their own geometric nature and the geometric relationship with the profile plane and the topographic line,
	we can distinguish between data that are constrained to lie in the topographic surface and others that are not
	linked to the topographic line in the profile.
	<br />In the latter case we have simple points (e.g. seismic hypocenters) and plane data with point-like extension
	at the profile scale (e.g. geological attitudes), that can be <u>projected</u> onto the profile plane irrespective
	of the topographic profile position.
	<br />In the former we have surficial line (e.g., fault traces) and polygonal features (e.g., geological
	outcrops) that are constrained to the topographic profile. Here we say that these geological features
	intersect the profile plane at the topography line.

	<div style="text-align:center;margin: 20px;font-size: 90%;font-style:italic;">
		<img src="images/f07.png" style="align:middle; max_width: 80%"/>
		<br /><br />Fig. 8. The plugin interface for geological data ingestion.
	</div>

	<h3>4.1 Point projections</h3>

	<div>
		When considering point data projected onto the profile plane, an obvious example is that of seismological
		data such as the hypocenters of a seismic sequence.
		<br />To plot this kind of data, we need to define the source layer, the field storing the elevation value
		for each point, the maximum allowed orthogonal distance of the point from the profile plane and the field
		that will be used to label the points when useful (fig. 9).
	</div>

	<br />
	<div style="text-align:center;margin: 20px;font-size: 90%;font-style:italic;">
		<img src="images/f09.png" style="align:middle; max_width: 80%"/>
		<br /><br />Fig. 9. Example of point projections UI.
	</div>

	<div>
		By double-clicking the "Plot profiles" item and fine-tuning the plot parameters from the "Graphical parameters"
		we obtain a profile where the points are plotted normally to the profile plane, with the height extracted
		by the "Z field"-defined values (fig. 10).
	</div>
	<br />
	<div style="text-align:center;margin: 20px;font-size: 90%;font-style:italic;">
		<img src="images/f10.png" style="align:middle; max_width: 80%"/>
		<br /><br />Fig. 10. Seismic hypocenters of the 2007 Colfiorito sequence (Central Apennines) plotted in a profile with parameters as in fig. 9.
	</div>

	<h3>4.2 Attitude projections</h3>

		<div>
			The case for attitude projections is similar to the point projections but it adds also a local plane-like
			nature for the projected data.
			<br />The source for geological attitudes is therefore still a point layer, but we need also to define the
			sources for the plane attitude, as given by its dip direction (or strike azimuth, following
			the Right-Hand-Rule) and the dip angle. Moreover, to limit the distance of the attitudes
			from the profile plane we set the "Max. profile distance" value and, when labelling data,
			the chosen "Id" field values will be used.

		</div>

		<div style="text-align:center;margin: 20px;font-size: 90%;font-style:italic;">
			<img src="images/f11.png" style="align:middle; max_width: 80%"/>
			<br /><br />Fig. 11. Geological attitudes projection UI.
		</div>

		<div>
			The geological attitudes are projected on the section plane perpendicular to the profile line,
			and are represented by a marker and a short segment representing the intersection between
			the geological plane and the profile plane (example in fig. 12).
		</div>
		<br />
		<div style="text-align:center;margin: 20px;font-size: 90%;font-style:italic;">
			<img src="images/f12.png" style="align:middle; max_width: 80%"/>
			<br /><br />Fig. 12. Example of geological attitudes projection along a profile in the eastern sector of Mt. Alpi zone (Basilicata, Southern Italy).
		</div>

	<h3>4.3 Line intersections</h3>

		<div>
			Line intersections is the first of the two cases of intersections between features and profile plane.
			Line features, such as the surficial traces of faults, plot as points lying on the topographic profile.
			We have to define just two elements: the input layer representing the features and a field storing
			the id/code/category for each feature, to be used when labelling them in the profile (fig. 13).

		</div>
		<div style="text-align:center;margin: 20px;font-size: 90%;font-style:italic;">
			<img src="images/f13.png" style="align:middle; max_width: 80%"/>
			<br /><br />Fig. 13. Line intersections UI.
		</div>

		<div>
			In fig. 14 the faults are represented in the plot as labelled triangles, plus the previously described
			seismic hypocenters (projected as points). It is therefore possible to reason on the relationships
			between active seisms and the mapped fault traces.
		</div>
		<br />
		<div style="text-align:center;margin: 20px;font-size: 90%;font-style:italic;">
			<img src="images/f14.png" style="align:middle; max_width: 80%"/>
			<br /><br />Fig. 14. Example of labelled fault intersections, together with seismic hypocenters of the 2007 Colfiorito sequence (Central Apennines).
		</div>

	<h3>4.4 Polygon intersections</h3>

		<div>
			Last case is the intersection of polygons lying on the topographic surface with the profile plane.
			As is the linear intersection case, we must just define two parameters: the input polygonal layer
			and a code/category field whose values will be used to categorize the intersections (fig. 15).
		</div>

		<br />
		<div style="text-align:center;margin: 20px;font-size: 90%;font-style:italic;">
			<img src="images/f15.png" style="align:middle; max_width: 80%"/>
			<br /><br />Fig. 15. Geological polygon intersection UI.
		</div>

		<div>
			In the following example (fig. 16) the features intersecting the profile plane fall into
			just two categories, whose symbolization is chosen by the user.
			Under each feature intersection its code/category is written, as an help for the visualization/analysis.
		</div>
		<div style="text-align:center;margin: 20px;font-size: 90%;font-style:italic;">
			<img src="images/f16.png" style="align:middle; max_width: 80%"/>
			<br /><br />Fig. 16. Example of geological polygon intersection on 3 parallel profiles in the Timpa San Lorenzo zone (Calabria, Southern Italy).
		</div>

<h2>5. Graphical parameters</h2>

	<div>
		The graphical parameters window ("Graphical parameters" in figs 1. 3 and 8) allows to fine-tune the
		plot graphical parameters.
		It is possible to define the figure width and height (expressed in inches), the vertical exxageration value
		(1 means scale of horizontal axis equal to scale of vertical axis), and the maximum and minimum elevations (fig. 17).
		Values for the vertical exaggeration and minimum and maximum elevation in the plot are precomputed,  the user may however modify them.
		<br />
		It is possible to save the currently defined graphical parameters ("save graphical parameters" in fig. 1) and
		to load them ("load graphical parameters") in a subsequent session.
		A note of caution for the polygonal intersections case: loading an obsolete parameters file could cause errors
		when using modified category field or input values.

	</div>

    <br />
    <div style="text-align:center;margin: 20px;font-size: 90%;font-style:italic;">
        <img src="images/f17.png" style="align:middle; max_width: 80%"/>
        <br /><br />Fig. 17. The window for the definition of the graphical parameters.
    </div>

	<div>
		The user can change the color for the topographic profile by defining it via the "Topographic elevations"
		menu item (fig. 18).
    </div>

    <br />
    <div style="text-align:center;margin: 20px;font-size: 90%;font-style:italic;">
        <img src="images/f18.png" style="align:middle; max_width: 80%"/>
        <br /><br />Fig. 18. The window for the definition of the topographic profile color.
    </div>

	<div>
		The point and attitude projections, as well the line intersections, present almost
		the same interface, that allows to define marker parameters and the
		presence of labels (fig. 19).
    </div>

    <br />
    <div style="text-align:center;margin: 20px;font-size: 90%;font-style:italic;">
        <img src="images/f19.png" style="align:middle; max_width: 80%"/>
        <br /><br />Fig. 19. The window for the definition of the point projections.
    </div>

	<div>
		In the polygon intersections case, the user defines the colors to apply,
		categorized by the category field previously set (fig. 20).
    </div>
    <br />
    <div style="text-align:center;margin: 20px;font-size: 90%;font-style:italic;">
        <img src="images/f21.png" style="align:middle; max_width: 80%"/>
        <br /><br />Fig. 20. The window for the definition of the polygon intersections parameters.
    </div>

<br />
<h2>6. Result export</h2>

<div>
	Profile data can be exported by double clicking the "Export -> topographic profiles" menu item (fig. 1).
	The two available export formats as 3D point or line shapefiles (fig. 21).  The profile plots can be saved as figures
	from inside the plot window, using the usual Matplotlib save tool.
</div>
<br />
<div style="text-align:center;margin: 20px;font-size: 90%;font-style:italic;">
	<img src="images/f22.png" style="align:middle; max_width: 80%"/>
	<br /><br />Fig. 21. The plugin interface for profile result export.
</div>

<br /><br /><br />

<div style="font-size: 85%; font-style: italic;">
-----
<br /><br />
Doc version 2022-09-08, by Mauro Alberti - <a href="mailto:alberti.m65@gmail.com">alberti.m65@gmail.com</a>
</div>



</body>

</html>
