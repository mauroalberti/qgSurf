import numbers

import qgis.core

from gst.core.geometries.polygons import *


def qgismpolygon_to_polygons(
    geometry: qgis.core.QgsPolygon
) -> List[Polygon]:

    polygons = []

    for ndx, part in enumerate(geometry.constParts()):

        outer_ring_coordinates = []

        outer_ring = part.exteriorRing()

        for vert in outer_ring.vertices():
            outer_ring_coordinates.append((vert.x(), vert.y()))

        outer_ring_line = Ln(outer_ring_coordinates)

        num_rings = part.numInteriorRings()

        if num_rings == 0:

            inner_ring_lines = None

        else:

            inner_ring_lines =[]

            for ring_ndx in range(num_rings):

                inner_ring_coords = []

                inner_ring = part.interiorRing(ring_ndx)
                for vert in inner_ring.vertices():
                    inner_ring_coords.append((vert.x(), vert.y()))

                inner_ring_lines.append(Ln(inner_ring_coords))

        polygons.append(Polygon(outer_ring_line, inner_ring_lines))

    return polygons


def qgismpolygon_to_xy_list(
    geometry: qgis.core.QgsPolygon
) -> List[Tuple[numbers.Real, numbers.Real]]:

    xy_coords = []

    for ndx, part in enumerate(geometry.constParts()):

        outer_ring = part.exteriorRing()

        for vert in outer_ring.vertices():
            xy_coords.append((vert.x(), vert.y()))

        num_rings = part.numInteriorRings()

        if num_rings > 0:

            for ring_ndx in range(num_rings):

                inner_ring = part.interiorRing(ring_ndx)

                for vert in inner_ring.vertices():
                    xy_coords.append((vert.x(), vert.y()))

    return xy_coords


def polygon_geometries(
    layer,
) -> List[List[Tuple[numbers.Real, numbers.Real]]]:

    xy_coords = []

    if layer.selectedFeatureCount() > 0:
        features = layer.selectedFeatures()
    else:
        features = layer.getFeatures()

    for feature in features:

        geom = feature.geometry()

        xy_pairs = qgismpolygon_to_xy_list(
            geometry=geom)

        xy_coords.append(xy_pairs)

    return xy_coords
