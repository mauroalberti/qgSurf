
from PyQt5.QtWidgets import QMessageBox


def info_qt(parent, header, msg):
    """
    Displays an information window.

    :param parent:
    :param header:
    :param msg:
    :return:
    """

    QMessageBox.information(
        parent,
        header,
        msg
    )


def warn_qt(parent, header, msg):
    """
    Displays a warning window.

    :param parent:
    :param header:
    :param msg:
    :return:
    """

    QMessageBox.warning(
        parent,
        header,
        msg
    )


def error_qt(
    parent,
    header,
    msg
):
    """
    Displays an error_qt window.

    :param parent:
    :param header:
    :param msg:
    :return:
    """

    QMessageBox.critical(
        parent,
        header,
        msg
    )
