
from typing import List, Tuple, Dict

from PyQt5.QtGui import *
from PyQt5.QtWidgets import *


def make_qaction(
    tool_params: Dict,
    plugin_name: str,
    icon_folder: str,
    parent: 'QObject'
):
    """
    Creates a QAction instance.
    Expected keys in params dictionary:
        tool_name: the tool name, string;
        icon_name: the name of the icon, string;
        whtsths_dscr: the action description, string.
    Used for QGIS Python plugin.

    :param tool_params: QAction text parameters.
    :type tool_params: dictionary.
    :param plugin_name: name of the plugin.
    :type plugin_name: str.
    :param icon_folder: icon folder name (assume single nesting).
    :type icon_folder: str.
    :param parent: the parent widget.
    :type parent: QObject or null pointer.
    :return:
    """

    q_icon_path = f":/plugins/{plugin_name}/{icon_folder}/{tool_params['icon_name']}"

    geoproc = QAction(
        QIcon(q_icon_path),
        tool_params["module_name"],
        parent)

    geoproc.setWhatsThis(tool_params["whtsths_dscr"])

    return geoproc


def refresh_combobox(
        combobox: QComboBox,
        init_text: str,
        texts: List[str]
):
    """
    Updates a combo box content using a list of strings.

    :param combobox: the combobox to be updated
    :param init_text: the initial updated combo box element
    :param texts: the list of the texts used to fill the combo box
    :return:
    """

    combobox.clear()

    if len(texts) == 0:
        return

    if init_text:
        combobox.addItem(init_text)

    combobox.addItems(texts)


def qcolor2rgbmpl(qcolor: QColor) -> Tuple[float, float, float]:
    """
    Calculates the red, green and blue components of the given QColor instance.

    :param qcolor: the input QColor instance
    :type qcolor: QColor
    :return: the triplet of the three RGB color values
    :type: a tuple of three floats
    """

    red = qcolor.red() / 255.0
    green = qcolor.green() / 255.0
    blue = qcolor.blue() / 255.0

    return red, green, blue

