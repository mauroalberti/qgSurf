# -*- coding: utf-8 -*-


from .core import (
    Vec3,
    Fol,
    Lin,
    Pair,
    Fault,
    Group,
    PairSet,
    FaultSet,
    Cluster,
    StereoGrid,
    G,
    settings,
)

from .helpers import sind, cosd, tand, acosd, asind, atand, atan2d
from .plotting import StereoNet


__all__ = (
    "Vec3",
    "Fol",
    "Lin",
    "Pair",
    "Fault",
    "Group",
    "PairSet",
    "FaultSet",
    "Cluster",
    "StereoGrid",
    "G",
    "settings",
    "sind",
    "cosd",
    "tand",
    "acosd",
    "asind",
    "atand",
    "atan2d",
    "StereoNet",
)

__version__ = "0.7.0"
__author__ = "Ondrej Lexa"
__email__ = "lexa.ondrej@gmail.com"
