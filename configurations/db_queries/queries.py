
ID_SOL, DIP_DIR, DIP_ANG, DATASET, NOTES, MPT_LON, MPT_LAT, MPT_Z, CREAT_TIME = range(9)
ID_PT, FK_ID_SOL, PT_INT_ID, LON, LAT, Z = range(6)

solutions_flds_str = "id, dip_dir, dip_ang, dataset, notes, mpt_lon, mpt_lat, mpt_z, creat_time"
source_points_flds_str = "id, id_sol, pt_int_id, longitude, latitude, z"

select_from_template = "SELECT %s FROM {}"
generic_where_in_template = " WHERE {} IN ({})"

query_sol_id_template = "select {id} from {solutions} where {creat_time} = (select MAX({creat_time}) from {solutions})"

query_solutions_all_template = "SELECT {}, {} FROM {}"
query_solutions_selection_template = query_solutions_all_template + generic_where_in_template

points_infos_base_query = """
SELECT src_points.id, 
       src_points.id_sol, 
       src_points.pt_int_id, 
       src_points.longitude, 
       src_points.latitude, 
       src_points.z,
       solutions.mpt_lon,
       solutions.mpt_lat, 
       solutions.mpt_z,   
       src_points.distance,
       src_points.dist_x,
       src_points.dist_y,
       src_points.dist_z,
       solutions.dip_dir, 
       solutions.dip_ang, 
       solutions.dataset, 
       solutions.notes, 
       solutions.creat_time
FROM src_points 
INNER JOIN solutions
ON src_points.id_sol = solutions.id
"""

select_all_points_query = points_infos_base_query + """
ORDER BY src_points.id
"""

selected_points_query_template = points_infos_base_query + """
WHERE src_points.id_sol IN ({})
ORDER BY src_points.id
"""

select_records_ids = """
SELECT id
FROM solutions
ORDER BY id ASC
"""

select_solutions_query_template = """
SELECT dip_dir, dip_ang, dataset, notes, mpt_lon, mpt_lat, mpt_z, creat_time
FROM solutions
WHERE id = {}
"""

select_point_coordinates_query_template = """
SELECT longitude, latitude, z
FROM src_points
WHERE id_sol = {}
ORDER BY pt_int_id
"""

