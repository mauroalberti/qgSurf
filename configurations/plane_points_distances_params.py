
field_parameters_of_output_table = [
    dict(name='id', ogr_type="ogr.OFTInteger"),
    dict(name='long', ogr_type="ogr.OFTReal"),
    dict(name='lat', ogr_type="ogr.OFTReal"),
    dict(name='z', ogr_type="ogr.OFTReal"),
    dict(name='distance', ogr_type="ogr.OFTReal"),
]