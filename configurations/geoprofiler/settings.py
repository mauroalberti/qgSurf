
pt_num_threshold = 1e4

colors_addit = [
    "darkseagreen",
    "darkgoldenrod",
    "darkviolet",
    "hotpink",
    "powderblue",
    "yellowgreen",
    "palevioletred",
    "seagreen",
    "darkturquoise",
    "beige",
    "darkkhaki",
    "red",
    "yellow",
    "magenta",
    "blue",
    "cyan",
    "chartreuse"
]
