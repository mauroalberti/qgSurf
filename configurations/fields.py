ID_FLD_NM = 'id'
LON_FLD_NM = 'lon'
LAT_FLD_NM = 'lat'
Z_FLD_NM = 'z'
SRC_PT_LON_FLD_NM = 'src_pt_lon'
SRC_PT_LAT_FLD_NM = 'src_pt_lat'
SRC_PT_Z_FLD_NM = 'src_pt_z'
DIP_DIR_FLD_NM = 'dip_dir'
DIP_ANG_FLD_NM = 'dip_ang'
