
plugin_name = "qgSurf"
settings_name = "alberese"

icon_folder = "icons"
config_folder_name = "configurations"
plugin_params_flnm = "plugin.yaml"
db_params_flnm = "sqlite.yaml"
output_shapefile_params_flnm = "output.yaml"
